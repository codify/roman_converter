# Roman numerals

Given a number, print the roman equivalent of it using the rules explained on http://www.novaroma.org/via_romana/numbers.html

### Prerequisites

Homebrew. Refer to http://brew.sh/ for how to.


### Installing

There is a setup script that installs rbenv to set the correct ruby version and associated gems

`bin/setup`

If you prefer not to use rbenv

- Set the ruby version to 2.3.1
- `gem install bundler`
- `bundle install`

## Running the tests

Just run `rspec` from the project directory to run all the tests
